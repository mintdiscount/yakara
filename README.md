# README

Simple app that sends emails based on templates.

## Initial Setup

Run `bin/setup`.
Add the following lines to `environments/development.rb` to use a test mailer on localhost:
```
config.action_mailer.delivery_method = :test
host = 'localhost:3000'
config.action_mailer.default_url_options = { host: host, protocol: 'http' }
```

## Running the app

Run `bin/rails server` and visit `0.0.0.0:3000` in your browser.
Visit `0.0.0.0:3000/emails` to view, edit or remove records of the emails sent.
Visit `0.0.0.0:3000/templates` to add, edit, or remove email templates.

## Running tests

Run `bin/rails test` to run non-system tests. Run `bin/rails test:system` to run system tests.
System tests are set up for Chrome; if using Firefox instead, change `using: :chrome` to `using: :firefox` in `test/application_system_test_case.rb`.
