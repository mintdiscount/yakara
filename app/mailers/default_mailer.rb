class DefaultMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.default_mailer.send_email.subject
  #
  def send_email(address, message)
    @message = message

    mail to: address
  end
end
