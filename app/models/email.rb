class Email < ApplicationRecord
  validates :name, presence: true
  validates :address, presence: true,
                      format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :message, presence: true
end
