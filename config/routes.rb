Rails.application.routes.draw do
  resources :emails
  resources :templates

  root 'emails#new'
end
