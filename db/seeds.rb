# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Template.create(
  [{ title: 'Hello Message',
     content: "Hello,\n\n"\
     "Thank you for your call today.\n"\
     "Please contact us if you have any further questions.\n\n"\
     'Yakara' },
   { title: 'Goodbye Message',
     content: "Goodbye,\n\n"\
     "We did not enjoy your call today.\n"\
     "Please do not contact us again.\n\n"\
     'Yakara' },
   { title: 'Lorem Ipsum',
     content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,'\
     ' sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'\
     ' Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi'\
     ' ut aliquip ex ea commodo consequat. Duis aute irure dolor in'\
     ' reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla'\
     ' pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa'\
     ' qui officia deserunt mollit anim id est laborum.' }]
)
