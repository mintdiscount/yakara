require 'test_helper'

class DefaultMailerTest < ActionMailer::TestCase
  test "send" do
    email = emails(:one)
    mail = DefaultMailer.send_email(email.address, email.message)
    assert_equal "Send email", mail.subject
    assert_equal [email.address], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match email.message, mail.body.encoded
  end

end
