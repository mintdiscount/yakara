# Preview all emails at http://localhost:3000/rails/mailers/default_mailer
class DefaultMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/default_mailer/send
  def send
    email = Email.first
    DefaultMailer.send_email(email.address, email.message)
  end

end
