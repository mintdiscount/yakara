require 'test_helper'

class EmailTest < ActiveSupport::TestCase
  def setup
    @email = Email.new
  end

  test 'requires name' do
    @email.address = 'test'
    @email.message = 'test'
    assert_not @email.save, 'Saved without name'
  end

  test 'requires address' do
    @email.name = 'test'
    @email.message = 'test'
    assert_not @email.save, 'Saved without address'
  end

  test 'requires valid email address' do
    @email.name = 'test'
    @email.message = 'test'
    @email.address = 'invalid@'
    assert_not @email.save, 'Saved with invalid email address'
  end

  test 'requires message' do
    @email.name = 'test'
    @email.address = 'test'
    assert_not @email.save, 'Saved without message'
  end
end
