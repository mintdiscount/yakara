require 'test_helper'

class TemplateTest < ActiveSupport::TestCase
  test 'must have title' do
    template = Template.new
    template.content = 'test'
    assert_not template.save, 'Saved without title'
  end

  test 'must have content' do
    template = Template.new
    template.title = 'test'
    assert_not template.save, 'Saved without content'
  end
end
