require "application_system_test_case"

class EmailsTest < ApplicationSystemTestCase
  setup do
    @email = emails(:one)
  end

  test "visiting the index" do
    visit emails_url
    assert_selector "h1", text: "Emails"
  end

  test "creating a Email" do
    visit emails_url
    click_on "New Email"

    fill_in "Email", with: @email.address
    fill_in "Message", with: @email.message
    fill_in "Name", with: @email.name
    click_on "Send"

    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_text "Email was successfully created"
    click_on "Back"
  end

  test "changing selected template changes message" do
    visit emails_url
    click_on "New Email"

    template = templates(:one)
    select template.title, from: 'email_template'
    assert_equal template.content, find(id: 'email_message').value

    template = templates(:two)
    select template.title, from: 'email_template'
    assert_equal template.content, find(id: 'email_message').value

    click_on "Back"
  end

  test "updating a Email" do
    visit emails_url
    click_on "Edit", match: :first

    fill_in "Email", with: @email.address
    fill_in "Message", with: @email.message
    fill_in "Name", with: @email.name
    click_on "Send"

    assert_text "Email was successfully updated"
    click_on "Back"
  end

  test "destroying a Email" do
    visit emails_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Email was successfully destroyed"
  end
end
